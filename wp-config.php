<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress_user' );

/** MySQL database password */
define( 'DB_PASSWORD', '15203030' );

/** MySQL hostname */
define( 'DB_HOST', '3.216.50.235' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DNF%Yeh|3J`ZwIXPDnhMYa,@xi)ZG*N}74w}v/M <[@tD=?>eVd(82=u~&]T~:-j' );
define( 'SECURE_AUTH_KEY',  'z)wNr~?}s`JKRO9%.W>WyszKjZ>d,AvuCW.JY>JX1X~]j^{Kr#KQXM,Hc^z/o:Iv' );
define( 'LOGGED_IN_KEY',    'pN~Wa}z%`;V)m=g0q$[|Og(ttCb)l,vsQJ)8:gj5 TaawHcW8yYC@~O<_IfWxJ1B' );
define( 'NONCE_KEY',        'kE$jguqQ{zQaDz.+[IWj*>GE>X7`WGdA@!Q<0E*1wf$)5p+@2=KVd$-A`z~#2_Fv' );
define( 'AUTH_SALT',        'cPp)k;0Qjh8c>+KSe?Gll pk+Vz?K+}10ST[1O,w[?(4r#ZumUgw@mF*W;x*S~3}' );
define( 'SECURE_AUTH_SALT', 'Zfgebg@][?>;?Vvik)nbBcFWyQ]0ss.{a|@A,jfuG/j<d%sYCZnd 3>,PGUOJi|!' );
define( 'LOGGED_IN_SALT',   'qFGqIhuOS(4f Arx1JU4!xWF+# $<;}D=^GWoE6^rCppgu=/Gxozxr<x4PUmT_J4' );
define( 'NONCE_SALT',       '<,Ickde*Ji]4YH3bcC=ebqqL~]=-_0QgJ ]waIu.Te`}m7HnH,b5wn^OCwYr@v7i' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
